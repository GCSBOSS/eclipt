
const ERROR_MESSAGES = {
    'unknown-opt': err => `Option --${err.token} does not exist in this context`,
    'not-multi': err => `Option ${getOptDef(err.opt)} cannot be set more than once`,
    'flag-val': err => `Option ${getOptDef(err.opt)} cannot be assigned a value`,
    'missing-val': err => `Option ${getOptDef(err.opt)} requires a value but none was supplied`,
    'unknown-alias': err => `Option -${err.token} does not exist in this context`,
    'bad-args': err => `Required ${err.args.length} arguments but ${err.count} were supplied`,
    'unknown-command': err => `Command '${err.token}' does not exist in this context`,
    'no-action': () => `You must choose one of the available commands`
}

function getOptDef(opt){
    let r = (opt.alias ? '-' + opt.alias + ', ' : '') + '--' + opt.name;

    if(!opt.flag){
        opt.value = opt.value ?? 'value'
        r += ' <' + opt.value + '>';
    }

    return r;
}

function getErrorMessage(err){
    return ERROR_MESSAGES[err.kind](err);
}

function getUsageLine(spec){
    let r = 'Usage:\n    ';

    if(spec.path.length > 0)
        r += spec.path.join(' ') + ' ';

    r += spec.name;

    if(typeof spec.opts == 'object' && Object.keys(spec.opts).length > 0)
        r += ' [OPTIONS]';

    if(!spec.action)
        r += ' COMMAND';

    if(spec.action && Array.isArray(spec.args))
        r += ' ' + spec.args.map(a => '<' + a + '>').join(' ');
    else if(spec.action && typeof spec.args == 'string')
        r += ' [' + spec.args + ']...';

    return r + '\n\n';
}

function getCommandsHelp(spec) {
    let r = 'Commands:\n';

    let size = 0;
    for(const name in spec.commands){
        const cmd = spec.commands[name];
        cmd.name = name;
        size = Math.max(name.length, size);
    }

    for(const name in spec.commands)
        r += '    ' + name.padEnd(size, ' ') + '    ' +
            (spec.commands[name].description ?? '') + '\n';

    return r + '\n';
}

function getHelp(spec){
    let r = '\n';

    if(spec.description)
        r += spec.description + '\n\n'

    r += getUsageLine(spec);

    if(spec.opts){
        r += 'Options:\n';

        const defs = [];
        const descs = [];
        let defSize = 0;
        for(const name in spec.opts){
            const opt = spec.opts[name];
            opt.name = name;
            const d = (!opt.alias ? '    ' : '') + getOptDef(opt);
            defSize = Math.max(d.length, defSize);
            defs.push(d);
            descs.push(opt.description ?? '');
        }

        for(const d of defs)
            r += '    ' + d.padEnd(defSize, ' ') + '    ' + descs.shift() + '\n';

        r += '\n';
    }

    if(typeof spec.commands == 'object' && Object.keys(spec.commands).length > 0)
        r += getCommandsHelp(spec);

    return r;
}

function parseOption({ input, spec, tokens }){
    let val, opt = tokens.shift().substr(2);

    if(opt.indexOf('=') > -1)
        [ opt, val ] = opt.split(/=/);

    if(!(opt in spec.opts))
        throw { kind: 'unknown-opt', token: opt, spec };

    const optSpec = spec.opts[opt];
    optSpec.name = opt;

    if(!optSpec.multi && opt in input.opts)
        throw { kind: 'not-multi', opt: optSpec, spec };

    if(typeof val == 'string' && optSpec.flag)
        throw { kind: 'flag-val', opt: optSpec, spec };

    if(optSpec.flag){
        input.opts[opt] = true;
        return;
    }

    if(typeof val != 'string'){
        if(!tokens[0] || tokens[0].charAt(0) == '-')
            throw { kind: 'missing-val', opt: optSpec, spec };
        val = tokens.shift();
    }

    if(optSpec.multi && opt in input.opts)
        (input.opts[opt] ).push(val );
    else if(optSpec.multi)
        input.opts[opt] = [ val  ];
    else
        input.opts[opt] = val ;
}

function parseAlias({ tokens, spec }){

    const alias = tokens[0].substr(1);

    if(alias == 'h'){
        tokens[0] = '--help';
        return;
    }

    for(const name in spec.opts){
        const opt = spec.opts[name];

        if(opt.alias == alias){
            tokens[0] = '--' + name;
            return;
        }
    }

    throw { kind: 'unknown-alias', token: alias, spec };
}

function parseArgs({ input, spec, tokens }){
    let count = 0;
    while(tokens.length > 0){
        count++
        input.args.push(tokens.shift() );
    }

    if(Array.isArray(spec.args) && spec.args.length != count)
        throw { kind: 'bad-args', count, args: spec.args };
}

function parseGroup({ tokens }){

    const opts = tokens.shift().split('').slice(1).reverse();

    for(const o of opts)
        tokens.unshift('-' + o);
}

function parseUnknown(frame){

    const t = frame.tokens[0];

    if(t == '--help')
        throw { kind: 'help', output: getHelp(frame.spec) };

    if(t && t.substr(0, 2) != '--' && t.charAt(0) == '-'){

        if(t.length > 2)
            parseGroup(frame);
        else
            parseAlias(frame);
        return parseUnknown(frame);
    }

    if(t && t != '--' && t.substr(0, 2) == '--'){
        parseOption(frame);
        return parseUnknown(frame);
    }

    if(t && frame.spec.commands && t in frame.spec.commands){
        const name = frame.tokens.shift() ;
        const newSpec = frame.spec.commands[name];
        newSpec.path = frame.spec.path.concat(frame.spec.name );
        return parseCommand(name, newSpec, frame.tokens, frame.input);
    }

    if(t && frame.spec.commands && !frame.spec.action)
        throw { kind: 'unknown-command', token: t, spec: frame.spec };

    if(t == '--')
        frame.tokens.shift();

    if(!frame.spec.action)
        throw { kind: 'no-action', spec: frame.spec };

    parseArgs(frame);

    return frame.spec.action(frame.input);
}

function parseCommand(name, spec, tokens, parent){

    const frame = {
        input: { name, opts: {}, args: [], parent }, tokens, spec
    };

    // TODO validate repeated aliases
    frame.spec.opts = frame.spec.opts || {};
    frame.spec.commands = frame.spec.commands || {};
    frame.spec.name = name;

    try{
        return parseUnknown(frame);
    }
    catch(err){

        if(!err.kind)
            throw err;

        if(!err.output){

            err.output = '\n\u001b[38;5;203m' + getErrorMessage(err) +
                '\u001b[0m\n\n' + getUsageLine(frame.spec);

            if(err.kind == 'unknown-command' || err.kind == 'no-action')
                err.output += getCommandsHelp(frame.spec);

            err.output += 'For more information try --help\n';
        }

        if(err.kind != 'help')
            err.error = true;

        if(process.env.ENV != 'testing')
            console.log(err.output);
        return err;
    }
}

module.exports = {

    eclipt(name, spec, tokens){
        tokens = tokens ?? [ ...process.argv ];
        spec.path = [];
        return parseCommand(name, spec, tokens);
    }

}