const assert = require('assert');
const { eclipt } = require('../lib/main');

process.env.ENV = 'testing';

describe('Eclipt', function(){

    it('parse arguments', () => {
        const out = eclipt('my-tool', { action: i => i },
            [ 'not-cmd' ]
        );

        assert.strictEqual(out.args.length, 1);
    });

    it('fail when missing action', () => {
        const err = eclipt('my-tool', {  },
            [ ]
        );

        assert.strictEqual(err.kind, 'no-action');
    });

    it('fail when repeating option', () => {
        const err = eclipt('my-tool', { opts: { pa: { flag: true, alias: 'p' } } },
            [ '-p', '-p' ]
        );

        assert.strictEqual(err.kind, 'not-multi');
    });

    it('parse all as arguments after argument modifier', () => {
        const out = eclipt('my-tool', { action: i => i },
            [ '--', '-n', '--fake-opt', 'fakearg', 'arg1' ]
        );

        assert.strictEqual(out.args.length, 4);
    });

    it('parse option', () => {
        const out = eclipt('my-tool', { action: i => i, opts: {
            op1: { }
        } }, [ '--op1', 'foobar' ]);

        assert.strictEqual(out.opts.op1, 'foobar');
    });

    it('parse assigned option', () => {
        const out = eclipt('my-tool', { action: i => i, opts: {
            op1: { }
        } }, [ '--op1=foobar' ]);

        assert.strictEqual(out.opts.op1, 'foobar');
    });

    it('fail when given option is not specified', () => {
        const err = eclipt('my-tool', { opts: { foo: { flag: true } } },
            [ '--bar', 'arg' ]);
        assert.strictEqual(err.kind, 'unknown-opt');
        assert.strictEqual(err.token, 'bar');
    });

    it('fail when option value is not provided', () => {
        const err = eclipt('my-tool', { opts: { foo: { } } },
            [ '--foo' ]);
        assert.strictEqual(err.kind, 'missing-val');
        assert.strictEqual(err.opt.name, 'foo');
    });

    it('fail when sending assigned value to flag option', () => {
        const err = eclipt('my-tool', { action: i => i, opts: { foo: { flag: true } } },
            [ '--foo=a' ]);
        assert.strictEqual(err.kind, 'flag-val');
    });

    it('fail unknown alias', () => {
        const err = eclipt('my-tool',
            { action: i => i, opts: {} },
            [ '-o', 'arg1', 'arg2' ]);

        assert.strictEqual(err.kind, 'unknown-alias');
        assert.strictEqual(err.token, 'o');
    });

    it('parse aliased options', () => {
        const out = eclipt('my-tool',
            { action: i => i, opts: { foo: { value: 'thing', alias: 'o' } } },
            [ '-o', 'arg1', 'arg2' ]);

        assert.strictEqual(out.opts['foo'], 'arg1');
        assert.strictEqual(out.args[0], 'arg2');
    });

    it('parse grouped aliased options', () => {
        const out = eclipt('my-tool',
            { action: i => i, opts: {
                opt1: { flag: true, alias: 'o' },
                opt2: { flag: true, alias: 'f' }
            } },
            [ '-of', 'arg2' ]);

        assert.strictEqual(out.opts.opt1, true);
        assert.strictEqual(out.opts.opt2, true);
        assert.strictEqual(out.args[0], 'arg2');
    });

    it('parse array options', () => {
        const out = eclipt('my-tool',
            { action: i => i, opts: {
                opt1: { multi: true, value: 'thing', alias: 'o' }
            } },
            [ '-o', '1', '-o', '2', '-o', '3', 'arg2' ]);

        assert.strictEqual(out.opts.opt1.length, 3);
        assert.strictEqual(out.args[0], 'arg2');
    });

    it('accept variable number of args', () => {
        const spec = { action: i => i, args: 'foo' };
        let out = eclipt('my-tool', spec, [ 'foo', 'bar' ]);
        assert(!out.kind);
        out = eclipt('my-tool', spec, [ ]);
        assert(!out.kind);
        out = eclipt('my-tool', spec, [ 'bar' ]);
        assert(!out.kind);
    });

    it('fail when not meeting args spec', () => {
        const err = eclipt('my-tool', { action: i => i, args: [ 'hu' ] },
            [ 'foo', 'bar', 'baz', 'arg' ]);
        assert.strictEqual(err.kind, 'bad-args');
    });

    it('parse a specified command', () => {
        const out = eclipt('my-tool', {
            commands: { doit: { action: i => i } }
        }, [ 'doit', 'arg' ]);

        assert.strictEqual(out.name, 'doit');
        assert.strictEqual(out.args[0], 'arg');
    });

    it('fail when given command doesn\'t exist', () => {
        const err = eclipt('my-tool', {
            commands: { doit: { action: i => i } }
        }, [ 'not-cmd' ]);

        assert.strictEqual(err.kind, 'unknown-command');
    });

    it('help option', () => {
        const out = eclipt('my-tool', {
            description: 'Test description',
            opts: {
                opt1: { flag: true },
                opt2: { flag: true, alias: 'f' }
            },
            commands: { doit: { action: i => i } }
        }, [ '--help' ]);

        assert(/Usage\:/.test(out.output));
        assert(/Options\:/.test(out.output));
        assert(/Commands\:/.test(out.output));
    });

    it('help alias', () => {
        const out = eclipt('my-tool', {
            commands: {
                foo: {
                    description: 'Test description',
                    opts: {
                        opt1: { flag: true },
                        opt2: { flag: true, alias: 'f' }
                    }
                }
            }
        }, [ 'foo', '-h' ]);

        assert(/Test\ /.test(out.output));
        assert(/Usage\:/.test(out.output));
        assert(/Options\:/.test(out.output));
        assert(!/Commands\:/.test(out.output));
    });

    it('read ARGV when no args array is supplied', () => {
        const err = eclipt('my-tool', {});
        assert.strictEqual(err.kind, 'unknown-command');
    });

});